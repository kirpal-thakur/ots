<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('api')->post('staff/login' , 'APIControllers\AuthController@login');
Route::middleware('api')->post('user/signup' , 'APIControllers\AuthController@register');
Route::middleware('api')->post('user/login' , 'APIControllers\AuthController@login');
Route::middleware('api')->post('/socialLogin' , 'APIControllers\AuthController@socialLogin');
Route::middleware('api')->post('/registerationConfirmation' , 'APIControllers\AuthController@registerationConfirmation');

Route::group(['middleware' => ['auth:api']], function () {
	Route::get('/user', 'APIControllers\AuthController@users');
    Route::get('/rocket', 'APIControllers\RocketController@index');
    Route::post('/rocket', 'APIControllers\RocketController@favorites');
});

Route::resource('notes', 'APIControllers\NotesController', ['only' => [
    'index', 'store', 'update', 'destroy'
]]);

Route::middleware('api')->post('/projects', 'APIControllers\ProjectsController@store');
Route::middleware('api')->post('/skills', 'APIControllers\SkillsController@store');