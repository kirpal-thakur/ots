<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserSkills extends Model
{
    protected $table = 'user_skills';

    public static  function get() {
        
        $res = DB::table('notes')
                    ->select('id', 'content')
                    ->where('user_id', '=', $id)
                    ->get();
        
        
        return $res;
    }
    
    public function add($content) {
                
        DB::table($this->table)->insert([
            'user_id' => $content['user_id'],
            'skill_id' => $content['skill_id'],
            'created_at' => new \DateTime()
        ]);         
        
       return DB::getPdo()->lastInsertId();
    }
    
    public static  function editNote($note_id, $content) {
        
        $id = Auth::user()->id;
                
        DB::table('notes')
                ->where('user_id', '=', $id)
                ->where('id', '=', $note_id)
                ->update(['content' => $content]);      
        
        return array('message' => 'Note was changed');
    }
    
    public static  function deleteNote($note_id) {
        
        $id = Auth::user()->id;
                
        DB::table('notes')
                ->where('user_id', '=', $id)
                ->where('id', '=', $note_id)
                ->delete();      
        
        return array('message' => 'Note was deleted');
    }
}
