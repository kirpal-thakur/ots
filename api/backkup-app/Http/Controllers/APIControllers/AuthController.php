<?php

namespace App\Http\Controllers\APIControllers;

use Mail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
     use RegistersUsers;
        
    public function register(Guard $auth, Request $request) {
        $fields = ['email', 'password', 'username'];
        // grab credentials from the request
        $credentials = $request->only($fields);
        
        $validator = Validator::make(
            $credentials,
            [
                'username' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6',
            ]
            );
        if ($validator->fails())
        {
            return response($validator->messages());
        }
        
        $result = User::create([
            'name' => $credentials['username'],
            'email' => $credentials['email'],
            'password' => bcrypt($credentials['password']),
        ]);
        
        $result['token'] = $this->tokenFromUser($result['id']);        
        $this->sendRegsiterationMail($result);
        return response(['success' => true]);
    }

    
    public function sendRegsiterationMail($user){
        $token  = urlencode(encrypt($user['email']));
        $link   = config('app.clientApp.url') . 'registerationConfirmation?t='. $token;
        $from   = config('app.mail.from');
        Mail::send('emails.registerationConfirmation', ['user' => $user, 'link' => $link], function ($m) use ($user, $from) {
            $m->from( $from, 'No-reply');
            $m->to($user['email'], $user->name)->subject('Registeration Successfull1');
        });
    }
    
    
    protected function login(Request $request) {
        auth()->shouldUse('api');
        //grab credentials from the request
        $credentials = $request->only('email', 'password');
        $credentials['status'] = 1;   
        if (auth()->attempt($credentials)) {
            $result['data']['access_token'] = auth()->issue();
            $result['data']['email'] = $credentials['email'];
            $result['success'] = true;
           return response($result);
        }
    
        return response(['Invalid Credentials or Account inactive']);
    }

        
    public function socialLogin(Guard $auth, Request $request) {
        $fields = ['email', 'name', 'provider', 'provider_id'];
        $credentials = $request->only($fields);
        $user   = User::where('provider_id', $credentials['provider_id'])->first();
        
        if($user){
            $user['email'] = $user->email;        
            $user['token'] = $this->tokenFromUser($user->id);        
            return response($user->only(['email', 'token']));
        }

        $validator = Validator::make(
            $credentials,
            [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'provider' => 'required',
                'provider_id' => 'required',
            ]
        );
        
        if ($validator->fails())
        {
            return response($validator->messages());
        }
        
      

        $result = User::create([
            'name' => $credentials['name'],
            'email' => $credentials['email'],
            'provider' => $credentials['provider'],
            'provider_id' => $credentials['provider_id'],
            'status' => 1
        ]);
        
        $result['token'] = $this->tokenFromUser($result['id']);        

        return response($result->only(['email', 'token']));
    }
    
    public function tokenFromUser($id)
    {
        // generating a token from a given user.
        $user = User::find($id);
    
        auth()->shouldUse('api');
        // logs in the user
        auth()->login($user);
    
        // get and return a new token
        $token = auth()->issue();
    
        return $token;
    }
    public function users(Request $request){
        return $users = User::paginate(10);
    }
    public function registerationConfirmation(Request $request){
        $email =  urldecode(decrypt($request->input('token')));
        $user   = User::where('email', $email)->first();
        if($user){
            User::where('email', $email)->update(['status' => 1]);
            return response(['success' => true]);
        } else{
            return response(['success' => false]);
        }
    }
}
