<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Projects extends Model
{
    protected $table = 'projects';

    public static  function getList() {
        
        $id = Auth::user()->id;
        
        $res = DB::table('notes')
                    ->select('id', 'content')
                    ->where('user_id', '=', $id)
                    ->get();
        
        
        return $res;
    }
    
    public function add($content) {
        
        $user_id = auth()->user()->id;
                
        DB::table($this->table)->insert([
            'user_id' => $user_id,
            'name' => (isset($content['name'])) ? $content['name'] : '',
            'url' => (isset($content['url'])) ? $content['url'] : '',
            'video' => (isset($content['video'])) ? $content['video'] : '',
            'description' => (isset($content['description'])) ? $content['description'] : '',
            'technology' => (isset($content['technology'])) ? $content['technology'] : '',
            'price' => (isset($content['price'])) ? $content['price'] : '0',
            'installation_charges' => (isset($content['installation_charges'])) ? $content['installation_charges'] : '0',
            'delivery_time' => (isset($content['delivery_time'])) ? $content['delivery_time'] : '0',
            'created_at' => new \DateTime()
        ]);         
        
        return true;
    }
    
    public static  function editNote($note_id, $content) {
        
        $id = Auth::user()->id;
                
        DB::table('notes')
                ->where('user_id', '=', $id)
                ->where('id', '=', $note_id)
                ->update(['content' => $content]);      
        
        return array('message' => 'Note was changed');
    }
    
    public static  function deleteNote($note_id) {
        
        $id = Auth::user()->id;
                
        DB::table('notes')
                ->where('user_id', '=', $id)
                ->where('id', '=', $note_id)
                ->delete();      
        
        return array('message' => 'Note was deleted');
    }
}
