<?php

namespace App\Http\Controllers\APIControllers;

use App\Projects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProjectsController extends Controller
{
    
    function __construct()
    {
        //$this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $result = Notes::getList();
        
        return response($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = $request->only('name', 'description');
        $validator = Validator::make(
            $content,
            [
                'name' => 'required|max:255',
                'description' => 'required'
            ]
        );
        if ($validator->fails())
        {
            return response($validator->messages());
        }

        $project = new Projects(); 
        $result = $project->add($content);
        if($result){
            return response(['success' => true]);
        }
        return response(['success' => false]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $note_id)
    {
        $content = $request['content'];
        
        $result = Notes::editNote($note_id, $content);
        
        return response($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function destroy($note_id)
    {
        $result = Notes::deleteNote($note_id);
        
        return response($result);
    }
}
