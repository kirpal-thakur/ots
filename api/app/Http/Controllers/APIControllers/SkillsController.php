<?php

namespace App\Http\Controllers\APIControllers;

use App\Skills;
use App\UserSkills;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SkillsController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $result = Notes::getList();
        
        return response($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = $request->only('skill');
        $validator = Validator::make(
            $content,
            [
                'skill' => 'required|max:255'
            ]
        );
        if ($validator->fails())
        {
            return response($validator->messages());
        }

        
        $model = new Skills(); 
        $result     = $model->getBySkill($content['skill']);
        if($result){
            $id     = $result->id;
        } else{
            $id = $model->add($content);
        }


        $user_id = auth()->user()->id;
        $chk    = UserSkills::where('user_id', $user_id)->where('skill_id', $id)->first();
        if(!$chk){
            $userSkillsModel    = new UserSkills();
            $userSkillsModel->add(['user_id' => $user_id, 'skill_id' => $id]);
            return response(['success' => true]);
        } else{
            return response(['error' => 'Already exists']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $note_id)
    {
        $content = $request['content'];
        
        $result = Notes::editNote($note_id, $content);
        
        return response($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function destroy($note_id)
    {
        $result = Notes::deleteNote($note_id);
        
        return response($result);
    }
}
