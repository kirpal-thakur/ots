import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { GlobalService } from "./global.service";
import { ResponseBody } from "./response-body";

@Injectable()
export class ProjectService {
  constructor(private globalService: GlobalService, private http: HttpClient) {}

  public addProject(project: Object) {
    const headers = GlobalService.getHeaders();

    return this.http
      .post<ResponseBody>(
        this.globalService.apiHost + "/projects",
        JSON.stringify(project),
        {
          headers: headers
        }
      )
      .map(response => {
        if (response.success) {
        } else {
        }
        return response;
      })
      .catch(GlobalService.handleError);
  }
}
