export class Project {
  name: string;
  video: string;
  url: string;
  description: string;
  technology: string;
  price: number;
  supportPrice: number;
  installationCharges: number;
  deliveryTime: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
