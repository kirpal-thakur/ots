import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";

import { ProjectComponent } from "./project.component";
import { ProjectRoutingModule } from "./project-routing.module";

import { MomentModule } from "ngx-moment";

import { ProjectService } from "../model/project.service";

@NgModule({
  imports: [CommonModule, SharedModule, ProjectRoutingModule, MomentModule],
  declarations: [ProjectComponent],
  providers: [ProjectService]
})
export class ProjectModule {}
