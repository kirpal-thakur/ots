import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";

import { ProjectService } from "../model/project.service";
import { Project } from "../model/project";

@Component({
  selector: "app-dashboard",
  templateUrl: "./project.component.html"
})
export class ProjectComponent implements OnInit {
  msg = "";
  addProjectForm: FormGroup;
  submitted = false;
  project: Project;

  constructor(
    private formBuilder: FormBuilder,
    private projectService: ProjectService
  ) {}

  ngOnInit() {
    this.addProjectForm = this.formBuilder.group({
      name: ["", Validators.required],
      video: [""],
      url: [""],
      description: ["", Validators.required],
      technology: ["", Validators.required],
      price: ["", Validators.required],
      supportPrice: ["", Validators.required],
      installationCharges: ["", Validators.required],
      deliveryTime: [""]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.addProjectForm.controls;
  }

  addProject() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addProjectForm.invalid) {
      return;
    }
    this.project = new Project(this.addProjectForm.value);
    this.projectService.addProject(this.project).subscribe(
      result => {
        if (result.success) {
          this.msg = "Project submitted successfully";
          //this.addProjectForm.reset();
        }
      },
      error => {}
    );
  }
}
