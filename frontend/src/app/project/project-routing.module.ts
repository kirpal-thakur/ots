import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ProjectComponent } from "./project.component";

const routes: Routes = [
  {
    path: "",

    data: {
      title: "Add Project"
    },
    children: [
      {
        path: "",
        pathMatch: "full",
        component: ProjectComponent,
        data: {
          title: "Account"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {}
