import { Component, OnInit } from "@angular/core";
import { UserService } from "../model/user.service";
import { ActivatedRoute, Params, Router } from "@angular/router";

@Component({
  selector: "app-confirm",
  templateUrl: "./confirm.component.html"
})
export class ConfirmComponent implements OnInit {
  submitted: boolean = false;
  errorMessage: string = "";
  isConfirmed: boolean = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userService.logout();

    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (typeof params["t"] !== "undefined") {
        const token = params["t"];
        this.onConfirm(token);
      } else {
        this.errorMessage =
          "The parameters are missing. Please check your access";
      }
    });
  }

  private onConfirm(token) {
    this.errorMessage = "";
    this.submitted = true;
    this.isConfirmed = false;

    this.userService.signupConfirm(token).subscribe(
      result => {
        if (result.success) {
          // show confirmation
          this.isConfirmed = true;
        } else {
          this.errorMessage =
            "Account confirmation is failed. Please check and try again.";
          this.submitted = false;
          this.isConfirmed = false;
        }
      },
      error => {
        if (typeof error.data.message !== "undefined") {
          try {
            const message = JSON.parse(error.data.message);
            let errorMessage = "";

            for (const m of Object.keys(message)) {
              errorMessage += message[m] + "\n";
            }

            this.errorMessage = errorMessage;
          } catch (e) {
            this.errorMessage = error.data.message;
          }
        } else {
          this.errorMessage = error.data;
        }

        this.submitted = false;
        this.isConfirmed = false;
      }
    );
  }
}
