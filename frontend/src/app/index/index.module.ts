import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { LoginModule } from "../login/login.module";

import { IndexRoutingModule } from "./index-routing.module";
import { IndexComponent } from "./index.component";

import { SignupComponent } from "../signup/signup.component";
import { LoginComponent } from "../login/login.component";
@NgModule({
  imports: [CommonModule, SharedModule, IndexRoutingModule, LoginModule],
  declarations: [IndexComponent, SignupComponent]
})
export class IndexModule {}
