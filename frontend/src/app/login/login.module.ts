import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { LoginComponent } from "./login.component";
import { LoginRoutingModule } from "./login-routing.module";
import {
  SocialLoginModule,
  AuthServiceConfig,
  AuthService
} from "angular5-social-auth";
import { getAuthServiceConfigs } from "../../conifg/social-api";

@NgModule({
  imports: [CommonModule, SharedModule, LoginRoutingModule, SocialLoginModule],
  declarations: [LoginComponent],
  providers: [
    { provide: AuthServiceConfig, useFactory: getAuthServiceConfigs },
    AuthService
  ],
  exports: [LoginComponent]
})
export class LoginModule {
  constructor() {}
}
