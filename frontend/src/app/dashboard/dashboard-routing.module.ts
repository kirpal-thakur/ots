import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from './dashboard.component';

const routes: Routes = [
    {
        path: '',

        data: {
            title: 'Dashboard'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: DashboardComponent,
                data: {
                    title: 'Account'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {
}
