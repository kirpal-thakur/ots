import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';

import {MomentModule} from 'ngx-moment';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        DashboardRoutingModule,
        MomentModule
    ],
    declarations: [DashboardComponent]
})
export class DashboardModule {
}
