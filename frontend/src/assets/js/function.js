$(function() {
		  $('.scroll-to-section , .ready-markit-area h4 a').click(function() {
			$(this).addClass("select"); 
			$(this).parent().siblings().find("a").removeClass("select");  
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top - 137
				}, 800);
				return false;
			  }
			}
		  });
		});
		
		
		jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 20,
            time: 2000
        });
    });
	
	
	 $(document).ready(function() {
  function setHeight() {
    windowHeight = $(window).innerHeight();
    $('.home-banner').css('min-height', windowHeight);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });
});



function resizeHeaderOnScroll() {
  const distanceY = window.pageYOffset || document.documentElement.scrollTop,
  shrinkOn = 50,
  headerEl = document.getElementById('js-header');
  
  if (distanceY > shrinkOn) {
    headerEl.classList.add("smaller");
  } else {
    headerEl.classList.remove("smaller");
  }
}

window.addEventListener('scroll', resizeHeaderOnScroll);