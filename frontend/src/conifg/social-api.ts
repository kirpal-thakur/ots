import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider
} from "angular5-social-auth";

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      //provider: new FacebookLoginProvider("2061482017243985")
      provider: new FacebookLoginProvider("2167012743581583")
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(
        "967906521709-dv07thpdn5pabd826nvnu69p8meebsoj.apps.googleusercontent.com"
      )
    },
    {
      id: LinkedinLoginProvider.PROVIDER_ID,
      provider: new LinkedinLoginProvider("81axlsipk397fr")
    }
  ]);
  return config;
}
